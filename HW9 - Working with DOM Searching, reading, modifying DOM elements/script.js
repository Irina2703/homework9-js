/* 
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
Об’єктна модель документа, або DOM, – це програмний інтерфейс (API), який дає змогу розробникам отримувати доступ до структури та вмісту HTML- або XML-документів. DOM являє собою документ у вигляді ієрархічного дерева елементів, де кожен елемент є об’єктом.
Модель DOM представляє структуру HTML або XML-документа у вигляді деревоподібної ієрархії. Кожен елемент у документі, такий як теги HTML або XML, представляється вузлом у цьому дереві. Вузли встановлюють зв’язки батько-потомок, утворюючи ієрархію елементів.



2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

Відмінності між
Властивість innerHTML повертає: текстовий вміст елемента, включаючи всі пробіли та внутрішні теги HTML. Властивість innerText повертає: лише текстовий вміст елемента та всіх його дочірніх елементів, без прихованих текстових інтервалів і тегів CSS, за винятком елементів <script> і <style>.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
Для пошуку елементів на сторінці застосовуються такі методи:

getElementById(value) : вибирає елемент, у якого атрибут id дорівнює value

getElementsByTagName(value) : вибирає всі елементи, у яких тег дорівнює value

getElementsByClassName(value) : вибирає всі елементи, які мають клас value

querySelector(value) : вибирає перший елемент, який відповідає css-селектору value

querySelectorAll(value) : вибирає всі елементи, які відповідають css-селектору value

4. Яка різниця між nodeList та HTMLCollection?
HTMLCollectionє динамічною структурою даних, а NodeList— статичною структурою даних. Тобто HTMLCollectionоновлюється щоразу, коли змінюється кількість елементів, одержаних за допомогою відповідного методу. А NodeListне змінюється після формування навіть якщо змінюється HTML-код сторінки.

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів. 
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */


// 1


// const featureElements = document.getElementsByClassName("feature");
// console.log(featureElements);


// const featureElements = document.querySelectorAll(".feature");
// console.log(featureElements);
// featureElements.style.textAlign = "center";

// 2

// const awesomeFeature = document.querySelectorAll("h2");


// awesomeFeature.forEach((elem) => {
//     elem.textContent = "Awesome feature"
// });
// console.log(awesomeFeature);


// 3

const featureTitleElements = document.querySelectorAll(".feature-title");
console.log(featureTitleElements);

featureTitleElements.forEach((elem) => {
    elem.textContent += "!"
});